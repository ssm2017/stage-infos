Ceci est une sorte de "wiki" collaboratif.
Si des personnes souhaitent ajouter des infos ou en supprimer, il suffit de cloner le depot et de proposer une merge request (ou pas).

Le sujet de ces documents est : informations utiles pour des techniciens en spectacle vivant et audiovisuel.